#pragma once
#include <iostream>
using namespace std;
//Klasa wykorzystana w Graf_lista
class Lista
{
public:
	class Element{ //klasa której obiekty sa przechowywane w liśie (krawędzie)
	public:
		Element *next;
		Element *prev;
		int poczatek;
		int wartosc;
		int waga;
		int ktora;
		
		Element(int a,int b,int c): next(NULL), prev(NULL){
			wartosc=a;
			waga=b;
			poczatek=c;
		}
		~Element(){
		}
		Element():next(NULL),prev(NULL), wartosc(NULL), waga(NULL){}
	};
	Element *head;
	

	int ile_krawedzi;
	Element* get_head();//zwróć głowę listy
	Lista();
	~Lista();
	bool search(int, int);//wyszukiwanie krawędzi o zadanym początku i końcu
	bool dodaj_koniec(int, int, int);//dodanie krawędzi na koniec
	void sortuj();//sortowanie bąbelkowe po wagach rosnąco
	void flip();//odwrócenie kolejności listy
	int pozycja(int);//zwróć pozycję elementu na liście
	bool search(int);//znajd� element o danej wartości
	void dodaj_koniec(Lista*);//dodaj zawartość innej listy do tej listy, nie zmieniając kolejności elementów listy z której kopiujemy
	void wyswietl(); //wyswietlenie
	bool dodaj(int, int, int);//dodaj krawędź na początek
	bool dodaj_za(int,int,int,int);//dodaj kraw�d� za elementem o podanej warto�ci
	void usun();//usuń krawędż z początku
	void dodaj(Lista*);//dodaj zawartość innej listy do tej listy, zmieniając kolejność elementów listy z której kopiujemy
	void swap(Element*, Element*);//zamień dwa elementy miejscami (do sortowania bąbelkowego)
	void destroy();//usuwanie przez destruktor
	
};