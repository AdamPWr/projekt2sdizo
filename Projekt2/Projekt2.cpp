// Projekt2.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <limits.h>

#include "Graf_macierz.h"
#include "Graf_lista.h"
#include <math.h>
#include "Timer.h"
#include <time.h>

using namespace std;

const int BLAD = 123456789; //wartość inicjalizacyjna dla algorytmów Prima/Kruskala (MAX_INT powoduje błędne działanie algorytmu)
//wyświetlenie macierzy o rozmiarze x (i kwadratowej o wymiarach x*x)
void wyswietl_tab(int **tab, int x)
{
	cout << endl;
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < x; j++)
		{
			cout << setw(3) << tab[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}
void wyswietl_tab(int *tab, int x)
{
	for (int i = 0; i < x; i++)
	{
		cout << tab[i] << " ";
	}
	cout << endl;
}

Graf_lista *losuj_MST_lista(int wierzcholki, int krawedzie, bool skierowany)
{

	Graf_lista *wynik = new Graf_lista(wierzcholki, krawedzie, skierowany);

	int *wierz = new int[wierzcholki];
	for (int i = 0; i < wierzcholki; i++) //wypelnij tablice wierzcholki
	{
		wierz[i] = i;
	}
	for (int i = wierzcholki - 1; i >= 0; i--) //wymieszanie tablicy liczb
	{
		int j = rand() % (i + 1);
		int temp = wierz[j];
		wierz[j] = wierz[i];
		wierz[i] = temp;
	}

	// #POPRAWKI BĄCZEK TO SKASOWAŁ BO NIE CHCE LOSOWAĆ PRZY KONSTRUOWANIU
	/*
	//dodaj pierwsza krawedz
	wynik->dodaj_krawedz(wierz[0],wierz[1],rand()%(5*wynik->get_wiersze())+1);
	wynik->dodaj_krawedz(wierz[1],wierz[0],rand()%(5*wynik->get_wiersze())+1);
	for(int i=2;i<wierzcholki;i++)
	{
		//dodawaj kolejne krawedzie do losowych wierzcholkow
		int temp=wierz[rand()%i];
		
		wynik->dodaj_krawedz(temp,wierz[i],rand()%(5*wynik->get_wiersze())+1);
		wynik->dodaj_krawedz(wierz[i],temp,rand()%(5*wynik->get_wiersze())+1);
	}
	*/
	delete[] wierz;
	return wynik;
}
Graf_macierz *losuj_MST_macierz(int wierzcholki, int krawedzie, bool skierowany)
{

	Graf_macierz *wynik = new Graf_macierz(wierzcholki, krawedzie, skierowany);

	int *wierz = new int[wierzcholki];
	for (int i = 0; i < wierzcholki; i++)
	{
		wierz[i] = i;
	}
	for (int i = wierzcholki - 1; i >= 0; i--) //wymieszanie tablicy liczb
	{
		int j = rand() % (i + 1);
		int temp = wierz[j];
		wierz[j] = wierz[i];
		wierz[i] = temp;
	}

	// #POPRAWKI BĄCZEK TO SKASOWAŁ BO NIE CHCE LOSOWAĆ PRZY KONSTRUOWANIU
	//cout<<endl;
	/*
	wynik->dodaj_krawedz(wierz[0],wierz[1],rand()%(5*wynik->get_wiersze())+1);
	wynik->dodaj_krawedz(wierz[1],wierz[0],rand()%(5*wynik->get_wiersze())+1);
	for(int i=2;i<wierzcholki;i++)
	{
		//wynik->wyswietl();
		//cout<<endl;
		int temp=wierz[rand()%i];
		
		wynik->dodaj_krawedz(temp,wierz[i],rand()%(5*wynik->get_wiersze())+1);
		wynik->dodaj_krawedz(wierz[i],temp,rand()%(5*wynik->get_wiersze())+1);
	}
	*/
	delete[] wierz;
	return wynik;
}

Graf_lista *Prim(Graf_lista *graf)
{
	Graf_lista *wynik = new Graf_lista(graf->get_wiersze(), graf->get_wiersze() - 1, false); //stworz graf wynikowy
	int dodany_wierzcholek = 0;
	Lista *krawedzie = new Lista(); //zapamietywanie listy krawedzi
	while (wynik->get_ile_krawedzi() != wynik->get_kolumny())
	{

		krawedzie->dodaj(graf->get_lista()[dodany_wierzcholek]); //dodaj krawedzie wychodzace z jednego wierzcholka do listy i posortuj
		krawedzie->sortuj();

		while (wynik->get_lista()[krawedzie->get_head()->wartosc]->get_head() != NULL) //tak długo jak lista pod wierzcholkiem do ktorego chcemy dodać krawędż jest niepusta
		{
			krawedzie->usun();
		}

		wynik->dodaj_krawedz(krawedzie->get_head()->poczatek, krawedzie->get_head()->wartosc, krawedzie->get_head()->waga); //dodaj krawedz
		dodany_wierzcholek = krawedzie->get_head()->wartosc;
		krawedzie->usun();
	}
	cout << "Prim lista:" << endl;
	delete krawedzie;
	return wynik;
}
Graf_macierz *Prim(Graf_macierz *graf)
{
	Graf_macierz *wynik = new Graf_macierz(graf->get_wiersze(), graf->get_wiersze() - 1, false);
	int dodany_wierzcholek = 0;
	int koniec_wartosc;
	Lista *krawedz = new Lista();
	Krawedz *sprawdzana = new Krawedz();
	while (wynik->get_ile_krawedzi() != wynik->get_kolumny())
	{
		//dodanie krawedzi grafu wychodzacych z jednego wierzcholka do listy krawedzi 'krawedz'
		for (int i = 0; i < graf->get_ile_krawedzi(); i++) //dla ka�dej kraw�dzi
		{
			*sprawdzana = graf->get_macierz()[dodany_wierzcholek][i];
			if (sprawdzana->wartosc == 1) //sprawdź czy ma wartość 1
			{
				for (int j = 0; j < graf->get_wiersze(); j++) //znajd� koniec krawędzi
				{
					if (graf->get_macierz()[j][i].wartosc == 1 && j != dodany_wierzcholek)
					{
						koniec_wartosc = j;
						break;
					}
				}

				krawedz->dodaj(sprawdzana->poczatek, koniec_wartosc, sprawdzana->waga); //dodaj krawędź
			}
		}
		krawedz->sortuj();

		bool usun = false;
		do
		{
			usun = false;
			for (int j = 0; j < wynik->get_ile_krawedzi(); j++)
			{
				if (wynik->get_macierz()[krawedz->get_head()->wartosc][j].wartosc == 1) //jeśli badany wierzcho�ek(koniec krawędzi z listy 'krawedz') jest z czymś po��czony
				{
					usun = true;
					break;
				}
			}
			if (usun) // to usuń t� krawędż
			{
				krawedz->usun();
			}
		} while (usun);
		wynik->dodaj_krawedz(krawedz->get_head()->poczatek, krawedz->get_head()->wartosc, krawedz->get_head()->waga);
		dodany_wierzcholek = krawedz->get_head()->wartosc;
		krawedz->usun();
	}
	cout << "Prim macierz:" << endl;
	delete sprawdzana;
	delete krawedz;
	return wynik;
}
Graf_lista *Kruskal(Graf_lista *graf)
{
	Lista *temp = new Lista();
	bool *odwiedzono = new bool[graf->get_wiersze()];
	Graf_lista *wynik = new Graf_lista(graf->get_wiersze(), graf->get_wiersze() - 1, false);
	Lista *krawedzie = new Lista();
	//dodaj wszystkie krawedzie grafu do listy krawedzie
	for (int i = 0; i < graf->get_wiersze(); i++)
	{
		krawedzie->dodaj(graf->get_lista()[i]);
	}
	krawedzie->sortuj();

	while (wynik->get_ile_krawedzi() != wynik->get_kolumny())
	{
		for (int i = 0; i < graf->get_wiersze(); i++)
		{
			odwiedzono[i] = false;
		}
		delete temp;
		temp = new Lista();
		if (!wynik->BFS(temp, krawedzie->get_head()->poczatek, krawedzie->get_head()->wartosc, odwiedzono)) //sprawdz czy sa polaczone, jesli nie to dodaj
		{
			wynik->dodaj_krawedz(krawedzie->get_head()->poczatek, krawedzie->get_head()->wartosc, krawedzie->get_head()->waga);
		}
		krawedzie->usun();
	}
	cout << "Kruskal lista:" << endl;
	delete[] odwiedzono;
	delete temp;
	delete krawedzie;
	return wynik;
}
Graf_macierz *Kruskal(Graf_macierz *graf)
{
	Krawedz *sprawdzana = new Krawedz();
	Lista *temp = new Lista();
	bool *odwiedzono = new bool[graf->get_wiersze()];
	Graf_macierz *wynik = new Graf_macierz(graf->get_wiersze(), graf->get_wiersze() - 1, false);
	Lista *krawedz = new Lista();
	int koniec_wartosc;
	//dodanie krawedzi do listy krawedz
	for (int dodany_wierzcholek = 0; dodany_wierzcholek < graf->get_wiersze(); dodany_wierzcholek++)
	{

		for (int i = 0; i < graf->get_ile_krawedzi(); i++) //dla każdej krawędzi
		{
			*sprawdzana = graf->get_macierz()[dodany_wierzcholek][i];
			if (sprawdzana->wartosc == 1) //sprawdż czy ma wartość 1
			{
				for (int j = 0; j < graf->get_wiersze(); j++) //znajdź koniec krawędzi
				{
					if (graf->get_macierz()[j][i].wartosc == 1 && j != dodany_wierzcholek)
					{
						koniec_wartosc = j;
						break;
					}
				}
				krawedz->dodaj(sprawdzana->poczatek, koniec_wartosc, sprawdzana->waga); //dodaj krawędź
			}
		}
	}
	krawedz->sortuj();

	while (wynik->get_ile_krawedzi() != wynik->get_kolumny())
	{
		for (int i = 0; i < graf->get_wiersze(); i++)
		{
			odwiedzono[i] = false;
		}
		delete temp;
		temp = new Lista();
		if (!wynik->BFS(temp, krawedz->get_head()->poczatek, krawedz->get_head()->wartosc, odwiedzono)) //sprawdz czy sa polaczone, jesli nie to dodaj
		{
			wynik->dodaj_krawedz(krawedz->get_head()->poczatek, krawedz->get_head()->wartosc, krawedz->get_head()->waga);
		}
		krawedz->usun();
	}
	cout << "Kruskal macierz:" << endl;
	delete sprawdzana;
	delete temp;
	delete krawedz;
	delete[] odwiedzono;
	return wynik;
}

void Dijkstra(Graf_lista *graf, int *wyniki, int a, int b)
{

	int *pomocnicza = new int[graf->get_wiersze()];
	for (int i = 0; i < graf->get_wiersze(); i++)
	{
		pomocnicza[i] = -5;
	}
	//ustaw odleglosci na BLAD, odleglosc od poczatek na 0
	for (int i = 0; i < graf->get_wiersze(); i++)
	{
		wyniki[i] = BLAD;
	}
	wyniki[a] = 0;
	int nietykalni = 1; //ilość już sprawdzonych wierzchożków, tak żeby algorytm się nie "wracał"
	int ktory = a;
	while (nietykalni < graf->get_wiersze())
	{
		pomocnicza[nietykalni - 1] = ktory;
		int usuwany, minimum = INT_MAX;
		Lista::Element *temp = graf->get_lista()[ktory]->get_head();
		while (temp != NULL)
		{
			if (wyniki[temp->wartosc] > (wyniki[temp->poczatek] + temp->waga)) //relaksacja wierzchołków
			{
				wyniki[temp->wartosc] = (wyniki[temp->poczatek] + temp->waga);
			}
			temp = temp->next;
		}
		for (int i = 0; i < graf->get_wiersze(); i++) //sprawdzenie wierzcholka o najmniejszym priorytecie
		{
			bool nietykalny = false;
			for (int j = 0; j < graf->get_wiersze(); j++)
			{
				if (pomocnicza[j] == i)
				{
					nietykalny = true;
				}
			}
			if (!nietykalny)
			{
				if (minimum > wyniki[i])
				{
					minimum = wyniki[i];
					usuwany = i;
				}
			}
		}
		if (ktory == b) //jesli najmniejszy priorytet miał b (stał się nietykalny, nie będzie lepiej) to przerwij
		{
			break;
		}
		ktory = usuwany;
		nietykalni++;
	}
	delete[] pomocnicza;
	cout << "Dijkstra lista:" << endl;
}
void Dijkstra(Graf_macierz *graf, int *wyniki, int a, int b)
{
	//to samo co lista, z dokładnością do pozyskiwania elementów do dodawania krawędzi
	int *pomocnicza = new int[graf->get_wiersze()];
	for (int i = 0; i < graf->get_wiersze(); i++)
	{
		pomocnicza[i] = -5;
	}
	for (int i = 0; i < graf->get_wiersze(); i++)
	{
		wyniki[i] = BLAD;
	}
	wyniki[a] = 0;
	int nietykalni = 1;
	int ktory = a;
	while (nietykalni < graf->get_wiersze())
	{
		pomocnicza[nietykalni - 1] = ktory;
		int usuwany, minimum = INT_MAX;
		for (int i = 0; i < graf->get_ile_krawedzi(); i++)
		{
			if (graf->get_macierz()[ktory][i].wartosc == 1)
			{
				int koniec;
				for (int j = 0; j < graf->get_wiersze(); j++)
				{
					if (graf->get_macierz()[j][i].wartosc != 0 && ktory != j)
					{
						koniec = j;
						break;
					}
				}
				if (wyniki[koniec] > (wyniki[ktory] + graf->get_macierz()[ktory][i].waga))
				{
					wyniki[koniec] = (wyniki[ktory] + graf->get_macierz()[ktory][i].waga);
				}
			}
		}

		for (int i = 0; i < graf->get_wiersze(); i++)
		{
			bool nietykalny = false;
			for (int j = 0; j < graf->get_wiersze(); j++)
			{
				if (pomocnicza[j] == i)
				{
					nietykalny = true;
				}
			}
			if (!nietykalny)
			{
				if (minimum > wyniki[i])
				{
					minimum = wyniki[i];
					usuwany = i;
				}
			}
		}
		if (ktory == b)
		{
			break;
		}
		ktory = usuwany;
		//cout<<ktory;
		nietykalni++;
	}
	delete[] pomocnicza;
	cout << "Dijkstra macierz:" << endl;
}
void Bellman_Ford(Graf_lista *graf, int *wyniki, int a, int b)
{
	for (int i = 0; i < graf->get_wiersze(); i++)
	{
		wyniki[i] = BLAD;
	}
	wyniki[a] = 0;

	Lista *krawedzie = new Lista();
	for (int i = 0; i < graf->get_wiersze(); i++)
	{
		krawedzie->dodaj(graf->get_lista()[i]);
	}
	krawedzie->sortuj();
	//dla wszystkich wierzchołków poza jednym
	for (int i = 0; i < graf->get_ile_krawedzi() - 1; i++)
	{
		//dla wszystkich krawedzi
		Lista::Element *temp = krawedzie->get_head();
		while (temp != NULL)
		{
			if (wyniki[temp->wartosc] > (wyniki[temp->poczatek] + temp->waga)) //dokonaj relaksacji
			{
				wyniki[temp->wartosc] = (wyniki[temp->poczatek] + temp->waga);
			}
			temp = temp->next;
		}
	}
	delete krawedzie;
	cout << "Bellman_Ford lista:" << endl;
}
void Bellman_Ford(Graf_macierz *graf, int *wyniki, int a, int b)
{
	for (int i = 0; i < graf->get_wiersze(); i++)
	{
		wyniki[i] = BLAD;
	}
	wyniki[a] = 0;
	Lista *krawedz = new Lista();
	int koniec_wartosc;
	//dodaj krawędzie do listy krawedz
	for (int dodany_wierzcholek = 0; dodany_wierzcholek < graf->get_wiersze(); dodany_wierzcholek++)
	{

		for (int i = 0; i < graf->get_ile_krawedzi(); i++) //dla każdej krawędzi
		{
			Krawedz sprawdzana = graf->get_macierz()[dodany_wierzcholek][i];
			if (sprawdzana.wartosc == 1) //sprawdż czy ma wartość 1
			{
				for (int j = 0; j < graf->get_wiersze(); j++) //znajdż koniec krawędzi
				{
					if (graf->get_macierz()[j][i].wartosc != 0 && j != dodany_wierzcholek)
					{
						koniec_wartosc = j;
						break;
					}
				}
				krawedz->dodaj(sprawdzana.poczatek, koniec_wartosc, sprawdzana.waga); //dodaj krawędź
			}
		}
	}
	krawedz->sortuj();
	//to samo co dla listy
	for (int i = 0; i < graf->get_ile_krawedzi() - 1; i++)
	{
		Lista::Element *temp = krawedz->get_head();
		while (temp != NULL)
		{
			if (wyniki[temp->wartosc] > (wyniki[temp->poczatek] + temp->waga))
			{
				wyniki[temp->wartosc] = (wyniki[temp->poczatek] + temp->waga);
			}
			temp = temp->next;
		}
	}
	delete krawedz;
	cout << "Bellman_Ford macierz:" << endl;
}

void wypelnij_2_grafy(Graf_lista *lista, Graf_macierz *macierz, int procenty)
{
	cout << "Wypełniam 2!" << endl;
	srand(time(NULL));
	int ile_ma_byc = (lista->wiersze * (lista->wiersze - 1));
	if (!lista->skierowany)
	{
		ile_ma_byc /= 2;
	}
	ile_ma_byc = ceil((double)procenty * ile_ma_byc / 100.0); //wylicz ilosc krawedzi
	int i = lista->ile_krawedzi;
	while (lista->ile_krawedzi < ile_ma_byc) //dodawaj az bedzie ich wystarczajaco duzo
	{
		int poczatek = rand() % lista->wiersze;
		int koniec = rand() % lista->wiersze;
		int waga = rand() % (5 * lista->wiersze) + 1;

		lista->dodaj_krawedz(poczatek, koniec, waga);
		macierz->dodaj_krawedz(poczatek, koniec, waga);
	}
}

int main(int argc, char *argv[])
{
	Graf_lista *LISTA, *temp1;
	Graf_macierz *MACIERZ, *temp2;
	srand(time(0));
	string nazwa;
	fstream plik;

	int wypelnienie, krawedzie, wierzcholki;

	bool skierowany;
	int znak = 123, znak2 = 321;
	int *wyniki;
	int poczatek, koniec, waga;
	int ford_dfs, ford_dfs2, ford_bfs, ford_bfs2;
	LISTA = NULL;
	MACIERZ = NULL;
	while (znak != 0)
	{
		cout << "Podaj jaki problem chcesz rozwiazywac:\n1.MST\n2.Najkrotsza sciezka\n";
		cin >> znak;
		switch (znak)
		{
		case 1:

			znak2 = 321;
			skierowany = false;
			while (znak2 != 0)
			{
				cout << "Podaj jaka czynnosc chcesz wykonac:\n1.Wczytaj graf z pliku\n2.Wygeneruj graf losowo\n3.Wyswietl\n4.Algorytm Prima\n5.Algorytm Kruskala\n\n0.Wroc do menu glownego\n";
				cin >> znak2;
				switch (znak2)
				{
				case 1:
					delete LISTA;
					delete MACIERZ;
					LISTA = NULL;
					MACIERZ = NULL;
					cout << "Podaj nazwe pliku: ";
					cin >> nazwa;
					plik.open(nazwa);
					plik >> krawedzie;
					plik >> wierzcholki;
					LISTA = new Graf_lista(wierzcholki, krawedzie, skierowany);
					MACIERZ = new Graf_macierz(wierzcholki, krawedzie, skierowany);
					for (int i = 0; i < krawedzie; i++)
					{
						plik >> poczatek >> koniec >> waga;
						LISTA->dodaj_krawedz(poczatek, koniec, waga);
						MACIERZ->dodaj_krawedz(poczatek, koniec, waga);
					}
					plik.close();
					break;
				case 2:
					delete LISTA;
					delete MACIERZ;
					LISTA = NULL;
					MACIERZ = NULL;
					cout << "Podaj liczbe wierzcholkow:\n";
					cin >> wierzcholki;
					if (wierzcholki < 2)
					{
						wierzcholki = 2;
					}
					cout << "Podaj wypelnienie grafu:\n";
					cin >> wypelnienie;
					krawedzie = ceil((double)wypelnienie * wierzcholki * (wierzcholki - 1) / 200);
					if (krawedzie < wierzcholki - 1)
					{
						krawedzie = wierzcholki - 1;
					}
					LISTA = losuj_MST_lista(wierzcholki, krawedzie, skierowany);
					MACIERZ = losuj_MST_macierz(wierzcholki, krawedzie, skierowany);
					//LISTA->wypelnij_graf(wypelnienie);
					//MACIERZ->wypelnij_graf(wypelnienie);
					wypelnij_2_grafy(LISTA, MACIERZ, wypelnienie);
					break;
				case 3:
					LISTA->wyswietl();
					MACIERZ->wyswietl();
					break;
				case 4:
				{
					if (LISTA->get_ile_krawedzi() < 1 || MACIERZ->get_ile_krawedzi() < 1)
					{
						break;
					}
					Timer timer;
					timer.start();
					temp1 = Prim(LISTA);
					timer.stop();
					cerr << "# PrimL: wierzchołków: " << wierzcholki << " wypelnienie: " << wypelnienie << " czas " << timer.getTimeInMicroSec() << std::fixed << std::setprecision(0) << " [ms]" << endl;
					temp1->wyswietl();
					timer.start();
					temp2 = Prim(MACIERZ);
					timer.stop();
					cerr << "# PrimM: wierzchołków: " << wierzcholki << " wypelnienie: " << wypelnienie << " czas " << timer.getTimeInMicroSec() << std::fixed << std::setprecision(0) << " [ms]" << endl;
					temp2->wyswietl();
					delete temp1;
					delete temp2;
					break;
				}
				case 5:
				{
					if (LISTA->get_ile_krawedzi() < 1 || MACIERZ->get_ile_krawedzi() < 1)
					{
						break;
					}
					Timer timer;
					timer.start();
					temp1 = Kruskal(LISTA);
					timer.stop();
					cerr << "# KruskalL: wierzchołków: " << wierzcholki << " wypelnienie: " << wypelnienie << " czas " << timer.getTimeInMicroSec() << std::fixed << std::setprecision(0) << " [ms]" << endl;

					temp1->wyswietl();
					timer.start();
					temp2 = Kruskal(MACIERZ);
					timer.stop();
					cerr << "# KruskalM: wierzchołków: " << wierzcholki << " wypelnienie: " << wypelnienie << " czas " << timer.getTimeInMicroSec() << std::fixed << std::setprecision(0) << " [ms]" << endl;
					temp2->wyswietl();
					delete temp1;
					delete temp2;
					break;
				}
				default:
					break;
				}
				cout << endl;
			}
			delete LISTA;
			delete MACIERZ;
			LISTA = NULL;
			MACIERZ = NULL;

			break;
		case 2:
			delete LISTA;
			delete MACIERZ;
			znak2 = 321;
			skierowany = true;
			while (znak2 != 0)
			{
				cout << "Podaj jaka czynnosc chcesz wykonac:\n1.Wczytaj graf z pliku\n2.Wygeneruj graf losowo\n3.Wyswietl\n4.Algorytm Bellmana-Forda\n5.Algorytm Dijkstry\n\n0.Wroc do menu glownego\n";
				cin >> znak2;
				switch (znak2)
				{
				case 1:
					delete LISTA;
					delete MACIERZ;
					LISTA = NULL;
					MACIERZ = NULL;
					cout << "Podaj nazwe pliku: ";
					cin >> nazwa;
					plik.open(nazwa);
					plik >> krawedzie;
					plik >> wierzcholki;
					LISTA = new Graf_lista(wierzcholki, krawedzie, skierowany);
					MACIERZ = new Graf_macierz(wierzcholki, krawedzie, skierowany);
					for (int i = 0; i < krawedzie; i++)
					{
						plik >> poczatek >> koniec >> waga;
						LISTA->dodaj_krawedz(poczatek, koniec, waga);
						MACIERZ->dodaj_krawedz(poczatek, koniec, waga);
					}
					plik.close();
					break;
				case 2:
					delete LISTA;
					delete MACIERZ;
					LISTA = NULL;
					MACIERZ = NULL;
					cout << "Podaj liczbe wierzcholkow:\n";
					cin >> wierzcholki;
					if (wierzcholki < 2)
					{
						wierzcholki = 2;
					}
					cout << "Podaj wypelnienie grafu:\n";
					cin >> wypelnienie;
					krawedzie = ceil((double)wypelnienie * wierzcholki * (wierzcholki - 1) / 100);
					if (krawedzie < wierzcholki - 1)
					{
						krawedzie = wierzcholki - 1;
					}
					LISTA = losuj_MST_lista(wierzcholki, krawedzie, skierowany);
					MACIERZ = losuj_MST_macierz(wierzcholki, krawedzie, skierowany);
					//LISTA->wypelnij_graf(wypelnienie);
					//MACIERZ->wypelnij_graf(wypelnienie);
					wypelnij_2_grafy(LISTA, MACIERZ, wypelnienie);
					break;
				case 3:
					LISTA->wyswietl();
					MACIERZ->wyswietl();
					break;
				case 4:
				{
					if (LISTA->get_ile_krawedzi() < 1 || MACIERZ->get_ile_krawedzi() < 1)
					{
						break;
					}
					cout << "Podaj poczatek sciezki:\n";
					cin >> poczatek;
					cout << "Podaj koniec sciezki:\n";
					cin >> koniec;
					wyniki = new int[wierzcholki];
					Timer timer;
					timer.start();
					Bellman_Ford(LISTA, wyniki, poczatek, koniec);
					timer.stop();
					cerr << "# BelmanFordL: wierzchołków: " << wierzcholki << " wypelnienie: " << wypelnienie << " czas " << timer.getTimeInMicroSec() << std::fixed << std::setprecision(0) << " [ms]" << endl;

					//wyswietl_tab(wyniki, wierzcholki);
					cout << "! Lista: Koszt drogi do wierzchołka " << koniec << ": " << wyniki[koniec] << endl;
					timer.start();
					Bellman_Ford(MACIERZ, wyniki, poczatek, koniec);
					timer.stop();
					cerr << "# BelmanFordM: wierzchołków: " << wierzcholki << " wypelnienie: " << wypelnienie << " czas " << timer.getTimeInMicroSec() << std::fixed << std::setprecision(0) << " [ms]" << endl;
					cout << "! Macierz: Koszt drogi do wierzchołka " << poczatek << ": " << wyniki[koniec] << endl;

					// wyswietl_tab(wyniki, wierzcholki);
					delete[] wyniki;
					break;
				}
				case 5:
				{
					if (LISTA->get_ile_krawedzi() < 1 || MACIERZ->get_ile_krawedzi() < 1)
					{
						break;
					}
					cout << "Podaj poczatek sciezki:\n";
					cin >> poczatek;
					cout << "Podaj koniec sciezki:\n";
					cin >> koniec;
					wyniki = new int[wierzcholki];

					Timer timer;

					timer.start();
					Dijkstra(LISTA, wyniki, poczatek, koniec);
					timer.stop();
					cerr << "# DijkstraL: wierzchołków: " << wierzcholki << " wypelnienie: " << wypelnienie << " czas " << timer.getTimeInMicroSec() << std::fixed << std::setprecision(0) << " [ms]" << endl;

					cout << "! Lista: Koszt drogi do wierzchołka " << koniec << ": " << wyniki[koniec] << endl;

					timer.start();
					Dijkstra(MACIERZ, wyniki, poczatek, koniec);
					timer.stop();

					cerr << "# DijkstraM: wierzchołków: " << wierzcholki << " wypelnienie: " << wypelnienie << " czas " << timer.getTimeInMicroSec() << std::fixed << std::setprecision(0) << " [ms]" << endl;

					cout << "! Lista: Koszt drogi do wierzchołka " << koniec << ": " << wyniki[koniec] << endl;

					delete[] wyniki;
					break;
				}
				default:
					break;
				}
				cout << endl;
			}
			delete LISTA;
			delete MACIERZ;
			LISTA = NULL;
			MACIERZ = NULL;
			break;

		default:
			break;
		}

		system("sleep 1");
	}

	delete LISTA;
	delete MACIERZ;

	//system("sleep 2");
	return 0;
}
