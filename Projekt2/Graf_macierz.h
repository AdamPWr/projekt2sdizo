#pragma once
#include "Krawedz.h"
#include <iostream>
#include <iomanip>
#include "Lista.h"
class Graf_macierz
{
public:

	Krawedz **macierz;//dwuwymiarowa tablica obiektów typu krawędź
	int wiersze, kolumny, ile_krawedzi;
	bool skierowany;
	Graf_macierz(void);
	int get_wiersze();
	int get_kolumny();
	int get_ile_krawedzi();
	Krawedz** get_macierz();//zwróc macierz
	Graf_macierz(int, int, bool=false);//konstruktor - wierzchołki, krawędzie, czy_skierowany
	bool dodaj_krawedz(int, int, int);//dodanie krawędzi początek, koniec, waga
	bool dodaj_krawedz(int, int);//dodanie krawędzi początek, koniec, waga=0
	bool czy_jest(int, int);//sprawdzenie czy krawędź początek, koniec już jest w grafie
	void wyswietl();
	~Graf_macierz(void);
	Graf_macierz* kopiuj();//kopiowanie grafu
	void siec_residualna(int **, Graf_macierz*);//tworzenie sieci residualnej do algorytmu forda-fulkersona, int** to tablica gdzie zapisany jest przep�yw
	bool DFS(Lista*, int, int,bool*);//DFS rekurencyjny
	bool DFS2(Lista*, int, int,bool*);//DFS iteracyjny
	bool BFS(Lista*, int, int,bool*);//BFS
	void wypelnij_graf(int);//wypelnienie grafu o zadanej gęstości
};

