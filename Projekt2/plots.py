import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')

from matplotlib import pyplot as plt
import numpy as np
import json

# t = np.arange(0., 1., 0.1)
# plt.plot(t, t, 'r--', t, t**2, 'g--', t, t**3, 'b--', t, np.sqrt(t), 'y--')
# plt.show()   

plot_data_L = {}
plot_data_M = {}


with open('output_averages.txt') as file:
    data = json.load(file)
    for point in data:
        key = point['title'] + point['fill']

        plot_data = plot_data_L if 'L' in point['title'] else plot_data_M

        if key not in plot_data:
            plot_data[key] = {'x': [], 'y': []}

        plot_data[key]['x'].append(point['vertices'])
        plot_data[key]['y'].append(point['time'])


plt.figure(figsize=(10,5))

# Type 1 Prim/Kruskal plot
for key in plot_data_L.keys():
    if 'Kruskal' in key or 'Prim' in key:
        plt.plot(plot_data_L[key]['x'], plot_data_L[key]['y'], label=key)
        plt.legend(loc='upper left')
#plt.plot(plot_data_L['PrimL:25']['y'], plot_data_L['PrimL:25']['x'])
plt.xlabel('Ilość wierzchołków')
plt.ylabel('Czas wykonania [ms]')
plt.title('Wykres typ 1: Prim/Kruskal - Lista sąsiadów')
plt.savefig('plots/type_1_prim_kruskal_list.png', bbox_inches='tight')

plt.cla()

# Type 1 Prim/Kruskal plot
for key in plot_data_L.keys():
    if 'Dijkstra' in key or 'Belman' in key:
        plt.plot(plot_data_L[key]['x'], plot_data_L[key]['y'], label=key)
        plt.legend(loc='upper left')
#plt.plot(plot_data_L['PrimL:25']['y'], plot_data_L['PrimL:25']['x'])
plt.xlabel('Ilość wierzchołków')
plt.ylabel('Czas wykonania [ms]')
plt.title('Wykres typ 1: Dijkstra/Belman - Lista sąsiadów')
plt.savefig('plots/type_1_dijkstra_belman_list.png', bbox_inches='tight')

plt.cla()

# Type 1 Prim/Kruskal plot
for key in plot_data_M.keys():
    if 'Kruskal' in key or 'Prim' in key:
        plt.plot(plot_data_M[key]['x'], plot_data_M[key]['y'], label=key)
        plt.legend(loc='upper left')
#plt.plot(plot_data_L['PrimL:25']['y'], plot_data_L['PrimL:25']['x'])
plt.xlabel('Ilość wierzchołków')
plt.ylabel('Czas wykonania [ms]')
plt.title('Wykres typ 1: Prim/Kruskal - Macierz')
plt.savefig('plots/type_1_prim_kruskal_matrix.png', bbox_inches='tight')

plt.cla()

# Type 1 Prim/Kruskal plot
for key in plot_data_M.keys():
    if 'Dijkstra' in key or 'Belman' in key:
        plt.plot(plot_data_M[key]['x'], plot_data_M[key]['y'], label=key)
        plt.legend(loc='upper left')
#plt.plot(plot_data_L['PrimL:25']['y'], plot_data_L['PrimL:25']['x'])
plt.xlabel('Ilość wierzchołków')
plt.ylabel('Czas wykonania [ms]')
plt.title('Wykres typ 1: Dijkstra/Belman - Macierz')
plt.savefig('plots/type_1_dijkstra_belman_matrix.png', bbox_inches='tight')

plt.cla()

#################
# TYP 2
#################

# Type 2 Prim/Kruskal plot 25%
for key in plot_data_M.keys():
    if '25' not in key:
        continue
    if 'Prim' in key or 'Kruskal' in key:
        plt.plot(plot_data_M[key]['x'], plot_data_M[key]['y'], label=key)
        plt.legend(loc='upper left')

for key in plot_data_L.keys():
    if '25' not in key:
        continue
    if 'Prim' in key or 'Kruskal' in key:
        plt.plot(plot_data_L[key]['x'], plot_data_L[key]['y'], label=key)
        plt.legend(loc='upper left')
#plt.plot(plot_data_L['PrimL:25']['y'], plot_data_L['PrimL:25']['x'])
plt.xlabel('Ilość wierzchołków')
plt.ylabel('Czas wykonania [ms]')
plt.title('Wykres typ 2: Kruskal/Prim - Wypełnienie grafu 25%')
plt.savefig('plots/type_2_prim_kruskal_25.png', bbox_inches='tight')

plt.cla()


# Type 2 Prim/Kruskal plot 50%
for key in plot_data_M.keys():
    if '50' not in key:
        continue
    if 'Prim' in key or 'Kruskal' in key:
        plt.plot(plot_data_M[key]['x'], plot_data_M[key]['y'], label=key)
        plt.legend(loc='upper left')

for key in plot_data_L.keys():
    if '50' not in key:
        continue
    if 'Prim' in key or 'Kruskal' in key:
        plt.plot(plot_data_L[key]['x'], plot_data_L[key]['y'], label=key)
        plt.legend(loc='upper left')
#plt.plot(plot_data_L['PrimL:25']['y'], plot_data_L['PrimL:25']['x'])
plt.xlabel('Ilość wierzchołków')
plt.ylabel('Czas wykonania [ms]')
plt.title('Wykres typ 2: Kruskal/Prim - Wypełnienie grafu 50%')
plt.savefig('plots/type_2_prim_kruskal_50.png', bbox_inches='tight')

plt.cla()



# Type 2 Prim/Kruskal plot 75%
for key in plot_data_M.keys():
    if '75' not in key:
        continue
    if 'Prim' in key or 'Kruskal' in key:
        plt.plot(plot_data_M[key]['x'], plot_data_M[key]['y'], label=key)
        plt.legend(loc='upper left')

for key in plot_data_L.keys():
    if '75' not in key:
        continue
    if 'Prim' in key or 'Kruskal' in key:
        plt.plot(plot_data_L[key]['x'], plot_data_L[key]['y'], label=key)
        plt.legend(loc='upper left')
#plt.plot(plot_data_L['PrimL:25']['y'], plot_data_L['PrimL:25']['x'])
plt.xlabel('Ilość wierzchołków')
plt.ylabel('Czas wykonania [ms]')
plt.title('Wykres typ 2: Kruskal/Prim - Wypełnienie grafu 75%')
plt.savefig('plots/type_2_prim_kruskal_75.png', bbox_inches='tight')

plt.cla()



# Type 2 Prim/Kruskal plot 99%
for key in plot_data_M.keys():
    if '99' not in key:
        continue
    if 'Prim' in key or 'Kruskal' in key:
        plt.plot(plot_data_M[key]['x'], plot_data_M[key]['y'], label=key)
        plt.legend(loc='upper left')

for key in plot_data_L.keys():
    if '99' not in key:
        continue
    if 'Prim' in key or 'Kruskal' in key:
        plt.plot(plot_data_L[key]['x'], plot_data_L[key]['y'], label=key)
        plt.legend(loc='upper left')
#plt.plot(plot_data_L['PrimL:25']['y'], plot_data_L['PrimL:25']['x'])
plt.xlabel('Ilość wierzchołków')
plt.ylabel('Czas wykonania [ms]')
plt.title('Wykres typ 2: Kruskal/Prim - Wypełnienie grafu 99%')
plt.savefig('plots/type_2_prim_kruskal_99.png', bbox_inches='tight')

plt.cla()




# Type 2 pathfinding


# Type 2 Dijkstra/Bellman plot 25%
for key in plot_data_M.keys():
    if '25' not in key:
        continue
    if 'Dijkstra' in key or 'Belman' in key:
        plt.plot(plot_data_M[key]['x'], plot_data_M[key]['y'], label=key)
        plt.legend(loc='upper left')

for key in plot_data_L.keys():
    if '25' not in key:
        continue
    if 'Dijkstra' in key or 'Belman' in key:
        plt.plot(plot_data_L[key]['x'], plot_data_L[key]['y'], label=key)
        plt.legend(loc='upper left')
#plt.plot(plot_data_L['DijkstraL:25']['y'], plot_data_L['DijkstraL:25']['x'])
plt.xlabel('Ilość wierzchołków')
plt.ylabel('Czas wykonania [ms]')
plt.title('Wykres typ 2: Belman/Dijkstra - Wypełnienie grafu 25%')
plt.savefig('plots/type_2_dijkstra_belman_25.png', bbox_inches='tight')

plt.cla()


# Type 2 Dijkstra/Bellman plot 50%
for key in plot_data_M.keys():
    if '50' not in key:
        continue
    if 'Dijkstra' in key or 'Belman' in key:
        plt.plot(plot_data_M[key]['x'], plot_data_M[key]['y'], label=key)
        plt.legend(loc='upper left')

for key in plot_data_L.keys():
    if '50' not in key:
        continue
    if 'Dijkstra' in key or 'Belman' in key:
        plt.plot(plot_data_L[key]['x'], plot_data_L[key]['y'], label=key)
        plt.legend(loc='upper left')
#plt.plot(plot_data_L['DijkstraL:25']['y'], plot_data_L['DijkstraL:25']['x'])
plt.xlabel('Ilość wierzchołków')
plt.ylabel('Czas wykonania [ms]')
plt.title('Wykres typ 2: Belman/Dijkstra - Wypełnienie grafu 50%')
plt.savefig('plots/type_2_dijkstra_belman_50.png', bbox_inches='tight')

plt.cla()



# Type 2 Dijkstra/Bellman plot 75%
for key in plot_data_M.keys():
    if '75' not in key:
        continue
    if 'Dijkstra' in key or 'Belman' in key:
        plt.plot(plot_data_M[key]['x'], plot_data_M[key]['y'], label=key)
        plt.legend(loc='upper left')

for key in plot_data_L.keys():
    if '75' not in key:
        continue
    if 'Dijkstra' in key or 'Belman' in key:
        plt.plot(plot_data_L[key]['x'], plot_data_L[key]['y'], label=key)
        plt.legend(loc='upper left')
#plt.plot(plot_data_L['DijkstraL:25']['y'], plot_data_L['DijkstraL:25']['x'])
plt.xlabel('Ilość wierzchołków')
plt.ylabel('Czas wykonania [ms]')
plt.title('Wykres typ 2: Belman/Dijkstra - Wypełnienie grafu 75%')
plt.savefig('plots/type_2_dijkstra_belman_75.png', bbox_inches='tight')

plt.cla()



# Type 2 Dijkstra/Bellman plot 99%
for key in plot_data_M.keys():
    if '99' not in key:
        continue
    if 'Dijkstra' in key or 'Belman' in key:
        plt.plot(plot_data_M[key]['x'], plot_data_M[key]['y'], label=key)
        plt.legend(loc='upper left')

for key in plot_data_L.keys():
    if '99' not in key:
        continue
    if 'Dijkstra' in key or 'Belman' in key:
        plt.plot(plot_data_L[key]['x'], plot_data_L[key]['y'], label=key)
        plt.legend(loc='upper left')
#plt.plot(plot_data_L['DijkstraL:25']['y'], plot_data_L['DijkstraL:25']['x'])
plt.xlabel('Ilość wierzchołków')
plt.ylabel('Czas wykonania [ms]')
plt.title('Wykres typ 2: Belman/Dijkstra - Wypełnienie grafu 99%')
plt.savefig('plots/type_2_dijkstra_belman_99.png', bbox_inches='tight')

plt.cla()


