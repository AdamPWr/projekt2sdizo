import os

def generate_steering(first_or_second_problem, vertices, fill_percent, first_or_second_algo):
        output = ''

        output += str(first_or_second_problem)
        output += '\n'

        output += '2\n'
        output += f'{vertices}\n'
        output += f'{fill_percent}\n'

        output += f'{first_or_second_algo+3}\n'

        if first_or_second_problem == 2:
                output += '1\n'
                output += f'{vertices-1}\n'

        output += '0\n0\n'
        
        #print(output)
        #print('=================')
        os.system (f'echo "{output}" > steering')

def measure_time():
        return os.system(f'./a.out < steering 2>> output.txt > /dev/null')


os.system('rm output.txt')

for first_or_second_problem in [1, 2]:
    for vertices in [10, 20, 30, 40, 50]:
        for fills in [25, 50, 75, 99]:
                        for first_or_second_algo in [1,2]:
                                generate_steering(first_or_second_problem, vertices, fills, first_or_second_algo)
                                for _ in range(100):
                                    a = measure_time()
                                    if a != 0:
                                        print('NANI')
                                        exit(0)

    
