#pragma once
#include "Lista.h"
class Graf_lista
{
public:
	Lista** lista; //tablica list
	int wiersze, kolumny, ile_krawedzi;
	bool skierowany;

	int get_wiersze();
	int get_kolumny();
	int get_ile_krawedzi();
	Lista** get_lista();//zwróć wskaźnik na tablicę list
	Graf_lista(void);
	Graf_lista(int, int, bool=false);//konstruktor - wierzcholki, krawedzie, czy_skierowany
	bool dodaj_krawedz(int, int, int);//dodanie krawędzi początek, koniec, waga
	bool dodaj_krawedz(int, int);//dodanie krawędzi początek, koniec, waga=0
	bool czy_polaczone(int, int, bool*);
	bool czy_jest(int, int);//czy krawdż poczatek-koniec już jest w grafie
	void wyswietl();
	~Graf_lista(void);
	Graf_lista* kopiuj(); //kopiowanie grafu
	void siec_residualna(int **, Graf_lista*);//tworzenie sieci residualnej do algorytmu forda-fulkersona, int** to tablica gdzie zapisany jest przep�yw
	bool DFS(Lista*, int, int,bool*);//DFS rekurencyjny, nieu�ywany
	bool DFS2(Lista*, int, int,bool*);//DFS iteracyjny
	bool BFS(Lista*, int, int,bool*);//BFS
	void wypelnij_graf(int);//wypełnienie grafu o zadanej gęstości
};

