import subprocess
from tqdm import tqdm # Progress bar library
import pypandoc
import subprocess
import matplotlib.pyplot as plt

USE_CACHED = True
ITERATIONS = 2000
STYLE = 'styles/style4.css'

class MarkdownDocument:
    def __init__(self, filename, style_css='style2.css'):
        self.filename = filename
        self.file = open(filename, 'w')
        self.style_css = style_css


    def add_header(self, text, size=2):
        self.file.write(size * '#' + f' {text}\n\n')

    def add_image(self, image, alt_text=''):

        self.file.write(
            f'![{alt_text}]({image})\n\n'
        )

    def add_text(self, text):
        self.file.write( f'{text}\n\n')

    def save(self):
        print('# Saving..')
        self.file.close()

        print('# Generating PDF..')
        output = pypandoc.convert_file(
            self.filename,
            'html',
            format='md',
        )

        html_output = open(self.filename.replace('.md', '') + '.html', 'w')
        html_output.write(output)
        html_output.close()

        pdf_command = f'pandoc -t html5 --css {self.style_css} {self.filename} -o ' + self.filename.replace('.md', '') + '.pdf'
        print(f'# Calling `{pdf_command}`')
        stream = subprocess.Popen(
            #[executable_to_call, str(i)],
            pdf_command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True)
        output = stream.communicate(timeout=100)



document = MarkdownDocument('test.md', STYLE)


document.add_header('Projekt SDiZO 2', size=1)
document.add_header('Badanie struktur grafowych.')
document.add_header('Adam Cierniak, 241310', size=3)

document.add_text('<br>')

document.add_header('Wstęp')
document.add_header('Cel projektu', size=3)
document.add_text(
'''
Zbadanie efektywności algorytmów grafowych: wyznaczanie minimalnego drzewa rozpinającego oraz najkrótszych ścieżek w zależności od
rozmiaru instancji oraz sposobu reprezentacji w pamięci komputera: macierz sąsiedztwa, lista sąsiedztwa.
Program stworzony do badań został napisany w języku C++. Czas mierzony był za biblioteki chrono.
Liczby wierzchołków użyte do testowania to 10, 20, 30, 40, 50, natomiast gęstości grafów to 25%, 50%, 75%, 99%.
Do procesu generacji użyte zostały funkcje z biblioteki random, dzięki czemu liczy losowane były z rozkładem jednostajnym.
Wagi krawędzi losowane były z przedziału w przypadku wag nieujemnych, natomiast w przypadku wag ujemnych z przedziału <0, 100>.
Generacja grafu polegała na losowaniu wierzchołka początkowego, a następnie wierzchołka końcowego dopóki wylosuje się wartość wierzchołka niepołączonego już z wierzchołkiem początkowym.
'''
)

document.add_header('Zaimplementowane algorytmy', size=3)
document.add_text('''
* Prima -  algorytm zachłanny wyznaczający tzw. minimalne drzewo rozpinające (MDR). Mając do dyspozycji graf nieskierowany i spójny, tzn. taki w którym krawędzie grafu nie mają ustalonego kierunku oraz
 dla każdych dwóch wierzchołków grafu istnieje droga pomiędzy nimi, algorytm oblicza podzbiór E′ zbioru krawędzi E, dla którego graf nadal pozostaje spójny, ale suma kosztów wszystkich krawędzi zbioru E′
jest najmniejsza możliwa

* Kruskala - algorytm grafowy wyznaczający minimalne drzewo rozpinające dla grafu nieskierowanego ważonego, o ile jest on spójny. Innymi słowy, znajduje drzewo zawierające wszystkie wierzchołki grafu, którego waga jest najmniejsza możliwa.

* Dikstry - mając dany graf z wyróżnionym wierzchołkiem (źródłem) algorytm znajduje odległości od źródła do wszystkich pozostałych wierzchołków.
Łatwo zmodyfikować go tak, aby szukał wyłącznie najkrótszej ścieżki do jednego ustalonego wierzchołka, po prostu przerywając działanie w momencie dojścia do wierzchołka docelowego.

* Belmana Forda - algorytm służący do wyszukiwania najkrótszych ścieżek w grafie ważonym z wierzchołka źródłowego do wszystkich pozostałych wierzchołków


''')


document.add_header('Sposób testowania')
document.add_text(
'''
Każdy algorytm został przetestowany dla 10, 20, 30, 40 i 50 wierzchołków, z zapełnieniem 25, 50, 75 i 99-procentowym.

Wygenerowane zostało po 100 instancji każdego z probemów, a średnie poszczególnych czasów zostały naniesione na wykresy.


Program wyświetla każdy wygenerowany graf i rozwiązanie każdego algorytmu, można więc sprawdzić poprawność działania chodźby za pomocą ołówka i kartki papieru.

\n'''
)


document.add_header(
    'Wykresy - typ 1'
)

document.add_text('''

Osobne dla każdej reprezentacji grafu (macierz, lista).
8 linii, po jednej dla każdej pary (Typ algorytmu, gęstośc).

''')

img_name = 'plots/type_1_dijkstra_belman_list.png'
document.add_image(img_name)


img_name = 'plots/type_1_dijkstra_belman_matrix.png'
document.add_image(img_name)

img_name = 'plots/type_1_prim_kruskal_list.png'
document.add_image(img_name)


img_name = 'plots/type_1_prim_kruskal_matrix.png'
document.add_image(img_name)

document.add_text('<br><br><br><br><br><br><br><br><br><br><br><br>')

document.add_header(
    'Wykresy - typ 2'
)

document.add_text('''
Osobne dla każdej gęstości grafu - porównują wydajność macierzowej i listowej implementacji grafu.
''')

document.add_header('Wyszukiwanie ścieżki - Dijkstra/Belman-Ford', size=3)
img_name = 'plots/type_2_dijkstra_belman_25.png'
document.add_image(img_name)
img_name = 'plots/type_2_dijkstra_belman_50.png'
document.add_image(img_name)
img_name = 'plots/type_2_dijkstra_belman_75.png'
document.add_image(img_name)
img_name = 'plots/type_2_dijkstra_belman_99.png'
document.add_image(img_name)

document.add_text('<br><br><br><br><br><br><br><br><br><br><br><br>')
document.add_header('MST - Prim/Kruskal', size=3)

img_name = 'plots/type_2_prim_kruskal_25.png'
document.add_image(img_name)
img_name = 'plots/type_2_prim_kruskal_50.png'
document.add_image(img_name)
img_name = 'plots/type_2_prim_kruskal_75.png'
document.add_image(img_name)
img_name = 'plots/type_2_prim_kruskal_99.png'
document.add_image(img_name)



document.add_header(
    'Wnioski'
)

document.add_text(
'''
Testy algorytmów dały spodziewane wyniki, to znaczy takie jakie wynikały z ich złożoności obliczeniowej.
Podczas testowania czasu wykonania  algorytmu Dijkstry i Belmana-Forda, dla gęstości wypełnienia 99%, linie na wykresie 
nałożyły się na siebie. Próbowałem poprawić to poprzez zmianę rozdzielczości osi Y jednak wykres stawał się wtedy mało czytelny
dlatego zostawiłem to w stanie takim jak wyżej.
'''
)

document.save()
