import json

output = []
line = ''

with open('output.txt') as file:
    for i in range(80):
        line = file.readline().split()
        timeL = int(line[7])
        titleL = line[1]
        verticesL = line[3]
        fillL = line[5]


        line = file.readline().split()
        timeM = int(line[7])
        titleM = line[1]
        verticesM = line[3]
        fillM = line[5]
        for i in range(99):
            timeL += int(file.readline().split()[7])
            timeM += int(file.readline().split()[7])
        timeL /= 100
        timeM /= 100

        output.append({
            'title': titleL,
            'vertices': verticesL,
            'fill': fillL,
            'time': timeL
        })
        output.append({
            'title': titleM,
            'vertices': verticesM,
            'fill': fillM,
            'time': timeM
        })
        #time /= 100

with open('output_averages.txt', 'w') as file:
    json.dump(output, file, indent=4)