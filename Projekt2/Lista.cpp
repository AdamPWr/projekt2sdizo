
#include "Lista.h"
//Klasa wykorzystana w Graf_lista

Lista::Lista():head(NULL){
}

int Lista::pozycja(int temp){
	int i=0;
	Element*element=head;
	while(element!=NULL){
		if(element->wartosc==temp) break;
		element=element->next;
		i++;
	}
	return i;
}
bool Lista::search(int x){
	Element* temp=head;
	while(temp!=NULL){
		if(temp->wartosc==x) return true;
		temp=temp->next;
	}
	return false;
}

bool Lista::dodaj_za(int poczatek,int temp,int waga,int za_ktora){
	if(head==NULL){
		head=new Element(temp,waga, poczatek);
		ile_krawedzi=1;
	}else if(search(za_ktora)){
		if(search(poczatek, temp))
		{
			return false;
		}
		int temp3=pozycja(za_ktora)+1;//dodaję jeden żeby uzyskać pozycję za wartością
		Element *element=new Element(temp,waga, poczatek);
		Element *temp=head;
		if(temp3==0){
			element->next=head;
			head=element;
			ile_krawedzi++;
			return true;
		}
		for(int i=0;i<temp3-1;i++)// przejdę w odpowiednie miejsce listy
			temp=temp->next;
		element->next=temp->next;
		temp->next=element;
		ile_krawedzi++;
		return true;
	}
	else
	{
		return dodaj(poczatek,temp,waga);
	}

}
bool Lista::dodaj(int poczatek,int temp,int waga)
{
	if(head==NULL){
		head=new Element(temp,waga, poczatek);
		ile_krawedzi=1;
	}else{
		if(search(poczatek, temp))
		{
			return false;
		}
		Element*element=new Element(temp,waga, poczatek);
		element->next=head;
		head->prev=element;
		head=element;
		ile_krawedzi++;
	}
	
	return true;
}

void Lista::flip()
{
	Lista* backup=new Lista();
	backup->dodaj(this);//dodanie z zamianą kolejności elementów daje żądany efekt.
	while(this->get_head())
	{
		this->destroy();
	}
	this->dodaj_koniec(backup);
	delete backup;
	
}

bool Lista::dodaj_koniec(int poczatek, int temp, int waga)
{
	if(head==NULL)
	{
		head=new Element(temp,waga, poczatek);
		ile_krawedzi=1;
	}
	else
	{
		if(search(poczatek, temp))
		{
			return false;
		}
		Element* element=new Element(temp, waga, poczatek);
		Element * temp=head;
		while(temp->next!=NULL)
		{
			temp=temp->next;
		}
		temp->next=element;
		element->prev=temp;
		ile_krawedzi++;
	}
	return true;
}

void Lista::dodaj_koniec(Lista* temp)
{
	Element* temp2=temp->get_head();
	while(temp2!=NULL)
	{
		this->dodaj_koniec(temp2->poczatek, temp2->wartosc, temp2->waga);
		temp2=temp2->next;
	}
}

void Lista::dodaj(Lista* temp)
{
	Element* temp2=temp->get_head();
	while(temp2!=NULL)
	{
		this->dodaj(temp2->poczatek, temp2->wartosc, temp2->waga);
		temp2=temp2->next;
	}
}
void Lista::usun()
{
	if(head==NULL)
	{
		return;
	}
	else
	{
		Element* temp=head;
		if(head->next!=NULL) head->next->prev=head->prev;
		head=head->next;
		delete temp;
	}
	ile_krawedzi--;
}

void Lista::destroy(){
	Element*element=head;
	head=head->next;
	if(element==head) head=NULL;
	delete element;
	element=NULL;
}
void Lista::wyswietl(){
	if(head==NULL)
		cout<<"Nie ma zadnych krawedzi.";
	else{
		int i=0;
		Element *iter=head;
		do{
			cout<<iter->wartosc<<" "<<iter->waga<<"  ";
			iter=iter->next;
		}
		while(iter!=NULL);
	}
	cout<<endl;
}
void Lista::swap(Element* a, Element*b)
{
	if(a!=head) a->prev->next=b;
	b->prev=a->prev;
	a->prev=b;
	if(b->next!=NULL)b->next->prev=a;
	a->next=b->next;
	b->next=a;
	if(a==head) head=b;
}
void Lista::sortuj()
{
	bool swapped=true;
	Element*temp;
	while(swapped)
	{
		temp=head;
		swapped=false;
		while(temp->next!=NULL)
		{
			if(temp->waga>temp->next->waga)
			{
				swap(temp, temp->next);
				swapped=true;
				temp=temp->prev;
			}
			temp=temp->next;
		}
	}
}
bool Lista::search(int x,int y){
	Element* temp=head;
	while(temp!=NULL){
		if(temp->poczatek==x&&temp->wartosc==y) return true;
		temp=temp->next;
	}
	return false;
}
Lista::~Lista(){
		while(head) destroy();
	}

Lista::Element* Lista::get_head()
{
	return head;
}
