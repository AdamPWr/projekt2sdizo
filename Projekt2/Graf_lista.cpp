
#include "Graf_lista.h"
#include <math.h>
#include <SFML/Graphics.hpp>

#include <sys/types.h>
#include <unistd.h>

Graf_lista::Graf_lista(void)
{
}

Graf_lista::Graf_lista(int wiersz, int kolumna, bool skierowany)
{
	this->skierowany = skierowany;
	this->ile_krawedzi = 0;
	this->wiersze = wiersz;
	this->kolumny = kolumna;
	lista = new Lista *[wiersze];
	for (int i = 0; i < wiersze; i++)
	{
		lista[i] = new Lista();
	}
}
void Graf_lista::wypelnij_graf(int procenty)
{
	srand(0);
	int ile_ma_byc = (this->wiersze * (this->wiersze - 1));
	if (!this->skierowany)
	{
		ile_ma_byc /= 2;
	}
	ile_ma_byc = ceil((double)procenty * ile_ma_byc / 100.0); //wylicz ilosc krawedzi
	int i = this->ile_krawedzi;
	while (this->ile_krawedzi < ile_ma_byc) //dodawaj az bedzie ich wystarczajaco duzo
	{
		this->dodaj_krawedz(rand() % this->wiersze, rand() % this->wiersze, rand() % (5 * this->wiersze) + 1);
	}
}
bool Graf_lista::czy_jest(int poczatek, int koniec)
{
	for (int i = 0; i < wiersze; i++)
	{
		if (lista[i]->search(poczatek, koniec)) //sprawdz czy krawedz zaczyna sie w poczatek i ma wartosc -1 lub 1 w koniec
		{
			return true;
		}
	}
	return false;
}

bool Graf_lista::dodaj_krawedz(int poczatek, int koniec, int waga)
{
	// cout << "dodaje " << waga << endl;
	if (poczatek == koniec)
	{
		return false;
	}
	if (ile_krawedzi == kolumny)
	{
		return false;
	}
	bool zwroc = lista[poczatek]->dodaj(poczatek, koniec, waga);
	if (!skierowany)
	{
		lista[koniec]->dodaj(koniec, poczatek, waga);
	}
	if (zwroc)
		ile_krawedzi++;
	return zwroc;
}

bool Graf_lista::dodaj_krawedz(int poczatek, int koniec)
{
	return dodaj_krawedz(poczatek, koniec, 0);
}

void Graf_lista::wyswietl()
{
	if (this == NULL)
	{
		cout << "Nie ma zadnych krawedzi\n";
		return;
	}
	for (int i = 0; i < wiersze; i++)
	{
		cout << i << ": ";
		lista[i]->wyswietl();
		//std::cout<<std::endl;
	}

	auto pid = fork();


	if(pid > 0)
		return;
	//////////////////////
	//					//
	//	   GRAFIKA		//
	//					//
	//////////////////////

	sf::RenderWindow window(sf::VideoMode(800, 800), "Graf!");
	window.setFramerateLimit(30);

	sf::CircleShape *circles = new sf::CircleShape[wiersze];
	sf::Text * texts = new sf::Text[wiersze];

	auto fillColor = sf::Color(38, 50, 56);

	sf::Font font;
	font.loadFromFile("roboto.ttf");
	//sf::CircleShape shape(100.f);

	vector<sf::Vertex> lineStarts;
	vector<sf::Vertex> lineEnds;

	for (auto i = 0; i < wiersze; i++)
	{

		float x = 400 + cos((float)i / float(wiersze) * 6.0) * 300;
		float y = 400 + sin((float)i / (float)wiersze * 6.0) * 300;

		circles[i].setPosition(sf::Vector2f(
			x,
			y));

		circles[i].setOrigin(15, 15);

		circles[i].setFillColor((sf::Color::White));

		circles[i].setRadius(30.0f);

		texts[i].setString(std::to_string(i));
		texts[i].setPosition(x,y);
		texts[i].setFont(font);
		texts[i].setCharacterSize(30);
		texts[i].setFillColor(sf::Color::Red);
	}

	for (auto i = 0; i < wiersze; i++)
	{

		auto element = lista[i]->get_head();

		// cout << "==================" << endl;
		while (element != NULL)
		{

			lineStarts.push_back(sf::Vertex(circles[i].getPosition() + sf::Vector2(20.0f, 20.0f), sf::Color::Green));
			lineEnds.push_back(sf::Vertex(circles[element->wartosc].getPosition() + sf::Vector2(-5.f, -5.f), sf::Color::Red));
			// cout << i << " " << element->wartosc << endl;

		
			element = element->next;
		}
	}

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear(fillColor);

		for (int i = 0; i < lineStarts.size(); i++)
		{
			sf::Vertex line[2] =
				{
					lineStarts[i],
					lineEnds[i]};

			window.draw(line, 2, sf::Lines);
		}

		for (auto i = 0; i < wiersze; i++)
		{
			window.draw(circles[i]);
			window.draw(texts[i]);
		}

		

		window.display();
	}

	exit(0);
}

Graf_lista::~Graf_lista(void)
{
	for (int i = 0; i < wiersze; i++)
	{
		delete lista[i];
	}
	delete[] lista;
}

int Graf_lista::get_kolumny()
{
	return this->kolumny;
}

int Graf_lista::get_wiersze()
{
	return this->wiersze;
}

Lista **Graf_lista::get_lista()
{
	return this->lista;
}

int Graf_lista::get_ile_krawedzi()
{
	if (this == NULL)
	{
		return 0;
	}
	return this->ile_krawedzi;
}

bool Graf_lista::DFS(Lista *lista, int poczatek, int koniec, bool *odwiedzono) //nieu�ywany fragment kodu, patrz DFS2
{
	bool znaleziono = false;
	Lista *backup = new Lista();
	backup->dodaj(lista);
	Lista::Element *temp = this->lista[poczatek]->get_head();
	while (temp != NULL)
	{
		while (lista->get_head())
			lista->destroy();
		lista->dodaj(backup);
		if (temp->waga > 0 && odwiedzono[temp->wartosc] == false)
		{
			lista->dodaj(temp->poczatek, temp->wartosc, temp->waga);

			if (temp->wartosc == koniec)
			{
				znaleziono = true;
			}
			else
			{
				odwiedzono[temp->wartosc] = true;
				znaleziono = DFS(lista, temp->wartosc, koniec, odwiedzono);
			}
			if (znaleziono)
			{
				break;
			}
		}
		temp = temp->next;
	}
	delete backup;
	return znaleziono;
}

bool Graf_lista::BFS(Lista *lista, int poczatek, int koniec, bool *odwiedzono)
{
	Lista *kolejka = new Lista();
	bool znaleziono = false;
	Lista::Element *temp = this->lista[poczatek]->get_head(); //wez pierwsza krawaedz wychodzącą z począteku
	odwiedzono[poczatek] = true;
	do
	{
		while (temp != NULL)
		{

			if (temp->waga > 0 && odwiedzono[temp->wartosc] == false) //dodaj krawędzie wychodzące z badanego wierzchołka
			{
				kolejka->dodaj_koniec(temp->poczatek, temp->wartosc, temp->waga);
				lista->dodaj_koniec(temp->poczatek, temp->wartosc, temp->waga);
				odwiedzono[temp->wartosc] = true;
				if (temp->wartosc == koniec)
				{
					znaleziono = true;
					break;
				}
			}
			temp = temp->next;
		}
		temp = lista->get_head(); //weż pierwszą krawędż na liście
		if (temp != NULL)
			temp = this->lista[temp->wartosc]->get_head(); //jako nastąpny badany wierzchołek przypisz koniec pozyskanej w powyższzej linijce krawędzi
		lista->usun();									   //usuń tą krawędż z listy
	} while ((lista->get_head() != NULL || temp != NULL) && !znaleziono);
	while (lista->get_head()) //usuń całą zawartość listy
	{
		lista->destroy();
	}
	if (!znaleziono)
	{
		return false; //jeśli nie znaleziono to przerwij
	}

	kolejka->flip();										 //odwróc zawartośc kolejki
	temp = kolejka->get_head();								 //weż pierwszą krawędź (łączy się ona z poszukiwanym wierzchołkiem)
	lista->dodaj(temp->poczatek, temp->wartosc, temp->waga); //dodaj ją do listy
	int x = temp->poczatek;
	while (x != poczatek) //cofaj się do początku, usuwając krawędzie z kolejki i dodając je do listy jeżli są częcią ścieżki
	{
		kolejka->usun();
		if (kolejka->get_head()->wartosc == x)
		{
			temp = kolejka->get_head();
			lista->dodaj(temp->poczatek, temp->wartosc, temp->waga);
			x = temp->poczatek;
		}
	}
	delete kolejka;
	return true;
}
bool Graf_lista::DFS2(Lista *lista, int poczatek, int koniec, bool *odwiedzono)
{
	//to samo co BFS, tylko wybierany jest element za którym ma zostać dodany nastąpna krawędź na listę i kolejkę
	int za = -123;
	Lista *kolejka = new Lista();
	bool znaleziono = false;
	Lista::Element *temp = this->lista[poczatek]->get_head();
	odwiedzono[poczatek] = true;
	do
	{

		while (temp != NULL)
		{

			if (temp->waga > 0 && odwiedzono[temp->wartosc] == false)
			{
				kolejka->dodaj_za(temp->poczatek, temp->wartosc, temp->waga, za);
				lista->dodaj_za(temp->poczatek, temp->wartosc, temp->waga, za);
				za = temp->wartosc;
				odwiedzono[temp->wartosc] = true;

				if (temp->wartosc == koniec)
				{
					znaleziono = true;
					break;
				}
			}
			temp = temp->next;
		}
		temp = lista->get_head();
		if (temp != NULL)
			za = temp->wartosc;
		if (temp != NULL)
			temp = this->lista[temp->wartosc]->get_head();
		lista->usun();
	} while ((lista->get_head() != NULL || temp != NULL) && !znaleziono);

	while (lista->get_head())
	{
		lista->destroy();
	}
	if (!znaleziono)
	{
		return false;
	}

	kolejka->flip();

	temp = kolejka->get_head();
	while (temp->wartosc != koniec)
	{
		kolejka->usun();
		temp = kolejka->get_head();
	}
	lista->dodaj(temp->poczatek, temp->wartosc, temp->waga);
	int x = temp->poczatek;
	while (x != poczatek)
	{
		kolejka->usun();
		if (kolejka->get_head()->wartosc == x)
		{
			temp = kolejka->get_head();
			lista->dodaj(temp->poczatek, temp->wartosc, temp->waga);
			x = temp->poczatek;
		}
	}
	delete kolejka;
	return true;
}

Graf_lista *Graf_lista::kopiuj()
{
	Graf_lista *wynik = new Graf_lista(this->wiersze, 2 * this->ile_krawedzi, this->skierowany); //utworz nowy graf
	for (int i = 0; i < this->wiersze; i++)														 // przekopiuj krawedzie po kolei
	{
		//wynik->dodaj_krawedz(i,j,0)
		wynik->lista[i]->dodaj(this->lista[i]);
		//lista[i]->wyswietl();
	}
	//dla kazdej krawedzi, jesli nie istnieje krawedz jej przeciwna (koniec->poczatek) to dodaj taka krawedz z waga 0 - jest to konieczne w algorytmie forda-fulkersona
	for (int i = 0; i < this->wiersze; i++)
	{
		Lista::Element *temp = this->lista[i]->get_head();
		while (temp != NULL)
		{
			bool czy_jest = false;
			Lista::Element *temp2 = this->lista[temp->wartosc]->get_head();
			while (temp2 != NULL)
			{
				if (temp2->wartosc == i)
				{
					czy_jest = true;
					break;
				}
				temp2 = temp2->next;
			}
			if (!czy_jest)
			{
				wynik->lista[temp->wartosc]->dodaj(temp->wartosc, i, 0);
			}
			temp = temp->next;
		}
	}
	return wynik;
}

void Graf_lista::siec_residualna(int **przeplyw, Graf_lista *wzor)
{
	//wzor to zmienna przechowujaca poczatkowy graf
	//przeplyw to tabela skonstruowana w taki sposób , że przepływ[a][b] to przepływ od wierzchożka a do b (bezpośrednio przez krawędź je łączące)

	for (int i = 0; i < this->wiersze; i++)
	{
		Lista::Element *temp = this->lista[i]->get_head();
		Lista::Element *temp2 = wzor->lista[i]->get_head();
		while (temp != NULL)
		{
			temp->waga = temp2->waga - przeplyw[temp->poczatek][temp->wartosc];
			temp = temp->next;
			temp2 = temp2->next;
		}
	}
}