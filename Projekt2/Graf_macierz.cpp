#include<math.h>
#include "Graf_macierz.h"

//Algorytmy są analogiczne do tych dla Graf_lista
Graf_macierz::Graf_macierz(void)
{
}


Graf_macierz::Graf_macierz(int wiersz, int kolumna, bool skierowany)
{
	this->skierowany=skierowany;
	this->ile_krawedzi=0;
	this->wiersze=wiersz;
	this->kolumny=kolumna;
	macierz=new Krawedz* [wiersze];
	for(int i=0;i<wiersze;i++)
	{
		macierz[i]=new Krawedz [kolumny];
	}
}
void Graf_macierz::wypelnij_graf(int procenty)
{
	srand(0);
	int ile_ma_byc=(this->wiersze * (this->wiersze-1));
	if(!this->skierowany)
	{
		ile_ma_byc/=2;
	}
	ile_ma_byc=ceil((double)procenty*ile_ma_byc/100.0);
	int i=this->ile_krawedzi;
	while(this->ile_krawedzi<ile_ma_byc)
	{
		this->dodaj_krawedz(rand()%this->wiersze, rand()%this->wiersze, rand()%(5*this->wiersze)+1);
		//if(ile_krawedzi%250==0) cout<<ile_krawedzi<<endl;
	}
}
bool Graf_macierz::czy_jest(int poczatek, int koniec)
{
	for(int i=0;i<ile_krawedzi;i++)
	{
		if(macierz[poczatek][i].wartosc==1 && macierz[koniec][i].wartosc!=0) //sprawdz czy krawedz zaczyna sie w poczatek i ma wartosc -1 lub 1 w koniec
		{
			return true;
		}
	}
	return false;
}

bool Graf_macierz::dodaj_krawedz(int poczatek, int koniec, int waga)
{
	
	//cout << "dodaje " << waga << endl;
	if(poczatek==koniec)
	{
		return false;
	}
	if(ile_krawedzi==kolumny)
	{
		return false;
	}
	if(czy_jest(poczatek,koniec))
	{
		return false;
	}
	macierz[poczatek][ile_krawedzi].poczatek=poczatek;
	macierz[poczatek][ile_krawedzi].wartosc=1;
	macierz[poczatek][ile_krawedzi].waga=waga;

	if(skierowany)
	{
		macierz[koniec][ile_krawedzi].poczatek=poczatek;
		macierz[koniec][ile_krawedzi].wartosc=-1;
	}
	else
	{
		macierz[koniec][ile_krawedzi].poczatek=koniec;
		macierz[koniec][ile_krawedzi].wartosc=1;
	}
	
	macierz[koniec][ile_krawedzi].waga=waga;
	ile_krawedzi++;
	return true;
}

bool Graf_macierz::dodaj_krawedz(int poczatek, int koniec)
{
	return dodaj_krawedz(poczatek, koniec, 0);
}

void Graf_macierz::wyswietl()
{
	if(this==NULL)
	{
		cout<<"Nie ma zadnych krawedzi\n";
		return;
	}
	for(int i=0;i<wiersze;i++)
	{
		for(int j=0;j<ile_krawedzi;j++)
		{
			std::cout<<std::setw(5)<<macierz[i][j].wartosc;
			std::cout<<std::setw(5)<<macierz[i][j].waga;
		}
		std::cout<<std::endl;
	}

	
}

Graf_macierz::~Graf_macierz(void)
{
	for(int i=0; i<wiersze;i++)
	{
		delete [] macierz[i];
	}
	delete [] macierz;
}

int Graf_macierz::get_kolumny()
{
	return this->kolumny;
}

int Graf_macierz::get_wiersze()
{
	return this->wiersze;
}

Krawedz** Graf_macierz::get_macierz()
{
	return this->macierz;
}

int Graf_macierz::get_ile_krawedzi()
{
	if(this==NULL)
	{
		return 0;
	}
	return this->ile_krawedzi;
}



bool Graf_macierz::DFS(Lista* lista, int poczatek, int koniec,bool *odwiedzono)
{
	int ktory;
	bool znaleziono=false;
	Lista* backup=new Lista();
	backup->dodaj(lista);
	for(int i=0;i<ile_krawedzi;i++)
	{
		ktory=-5;
		while(lista->get_head()) lista->destroy();
		lista->dodaj(backup);

		if(macierz[poczatek][i].wartosc==1 && macierz[poczatek][i].waga>0)
		{
			for(int j=0;j<wiersze;j++)
			{
				if(macierz[j][i].wartosc==-1)
				{
					ktory=j;
					break;
				}
			}
		}

		if(ktory!=-5 && odwiedzono[ktory]==false)
		{
			lista->dodaj(poczatek, ktory, macierz[poczatek][i].waga);

			if(ktory==koniec)
			{
				znaleziono=true;
			}
			else
			{
				odwiedzono[ktory]=true;
				znaleziono=DFS(lista,ktory,koniec, odwiedzono);
			}
			if(znaleziono)
			{
				break;
			}
		}
	}
	delete backup;
	return znaleziono;
}

bool Graf_macierz::BFS(Lista* lista, int poczatek, int koniec,bool *odwiedzono)

{
	Lista* kolejka=new Lista();
	bool znaleziono=false;
	odwiedzono[poczatek]=true;
	Lista::Element* temp;
	int ktora=poczatek;
	int ktory=poczatek;
	
	do
	{
		for(int i=0;i<ile_krawedzi;i++)
		{
			if(macierz[ktory][i].wartosc==1 && macierz[ktory][i].waga>0)
			{
				for(int j=0;j<wiersze;j++)
				{
					if(macierz[j][i].wartosc!=0 && ktory!=j)
					{
						
						ktora=j;
						break;
					}
				}
			}
			if(odwiedzono[ktora]==false)
			{
				kolejka->dodaj_koniec(ktory, ktora, macierz[ktory][i].waga);
				lista->dodaj_koniec(ktory, ktora, macierz[ktory][i].waga);
				odwiedzono[ktora]=true;
				if(ktora==koniec)
				{
					znaleziono=true;
					break;
				}
			}
		}
		temp=lista->get_head();
		if(temp!=NULL) ktory=temp->wartosc;
		lista->usun();
	} while((lista->get_head()!=NULL ||temp!=NULL) && !znaleziono);
	
	/*kolejka->wyswietl();
	kolejka->flip();
	kolejka->wyswietl();*/
	while(lista->get_head())
	{
		lista->destroy();
	}
	if(!znaleziono)
	{
		return false;
	}
	
	kolejka->flip();
	temp=kolejka->get_head();
	lista->dodaj(temp->poczatek, temp->wartosc, temp->waga);
	int x=temp->poczatek;
	
	while(x!=poczatek)
	{
		//cout<<"asd"<<endl;
		//kolejka->wyswietl();
		//lista->wyswietl();
		//cout<<endl;
		kolejka->usun();
		if(kolejka->get_head()->wartosc==x)
		{
			temp=kolejka->get_head();
			lista->dodaj(temp->poczatek, temp->wartosc, temp->waga);
			x=temp->poczatek;
		}
	}
	delete kolejka;
	return true;
}
bool Graf_macierz::DFS2(Lista* lista, int poczatek, int koniec,bool *odwiedzono)
{
	int za=-123;
	Lista* kolejka=new Lista();
	bool znaleziono=false;
	odwiedzono[poczatek]=true;
	Lista::Element* temp;
	int ktora=poczatek;
	int ktory=poczatek;
	do
	{
		for(int i=0;i<ile_krawedzi;i++)
		{
			if(macierz[ktory][i].wartosc==1 && macierz[ktory][i].waga>0)
			{
				for(int j=0;j<wiersze;j++)
				{
					if(macierz[j][i].wartosc!=0 && ktory!=j)
					{
						ktora=j;
						break;
					}
				}
			}
			if(odwiedzono[ktora]==false)
			{
				kolejka->dodaj_za(ktory, ktora, macierz[ktory][i].waga,za);
				lista->dodaj_za(ktory, ktora, macierz[ktory][i].waga,za);
				za=ktora;
				odwiedzono[ktora]=true;
				if(ktora==koniec)
				{
					znaleziono=true;
					break;
				}
			}
		}
		temp=lista->get_head();
		if(temp!=NULL) za=temp->wartosc;
		if(temp!=NULL) ktory=temp->wartosc;
		lista->usun();
	} while((lista->get_head()!=NULL ||temp!=NULL) && !znaleziono);
	
	/*kolejka->wyswietl();
	kolejka->flip();
	kolejka->wyswietl();*/
	while(lista->get_head())
	{
		lista->destroy();
	}
	if(!znaleziono)
	{
		return false;
	}
	
	kolejka->flip();
	temp=kolejka->get_head();
	while(temp->wartosc!=koniec)
	{
		kolejka->usun();
		temp=kolejka->get_head();
	}
	lista->dodaj(temp->poczatek, temp->wartosc, temp->waga);
	int x=temp->poczatek;
	
	while(x!=poczatek)
	{
		//cout<<"asd"<<endl;
		//kolejka->wyswietl();
		//lista->wyswietl();
		//cout<<endl;
		kolejka->usun();
		if(kolejka->get_head()->wartosc==x)
		{
			temp=kolejka->get_head();
			lista->dodaj(temp->poczatek, temp->wartosc, temp->waga);
			x=temp->poczatek;
		}
	}
	delete kolejka;
	return true;
}


Graf_macierz* Graf_macierz::kopiuj()
{
	Graf_macierz* wynik=new Graf_macierz(this->wiersze, 2*this->ile_krawedzi,this->skierowany);
	int ktora;
	//analogicznie jak dla listy - dodaj krawedzie, jesli nie ma krawedzi przeciwnej do dodaj ja z waga 0
	for(int i=0;i<this->wiersze;i++)
	{
		for(int j=0;j<this->ile_krawedzi;j++)
		{
			if(this->macierz[i][j].wartosc==1)
			{
				for(int k=0;k<this->wiersze;k++)
				{
					if(macierz[k][j].wartosc==-1)
					{
						ktora=k;
						break;
					}

				}
				if(wynik->czy_jest(i,ktora)) //je�li ju� wcze�niej dodano kraw�d� o wadze 0 (sztuczn�)
				{
					for(int m=0;m<wynik->get_ile_krawedzi();m++) //znajd� t� kraw�d� izmie� jej wag�
					{
						if(wynik->macierz[i][m].wartosc==1 && wynik->macierz[ktora][m].wartosc==-1)
						{
							wynik->macierz[i][m].waga=macierz[i][j].waga;
							wynik->macierz[ktora][m].waga=macierz[ktora][j].waga;
						}
					}
				}
				else //je�li nie dodano to dodaj prawdziw� kraw�d� i odpowiadaj�c� jej sztuczn�
				{
					wynik->dodaj_krawedz(i,ktora,this->macierz[i][j].waga);
					wynik->dodaj_krawedz(ktora, i,0);
				}
				
			}
		}
	}
	return wynik;
}

void Graf_macierz::siec_residualna(int **przeplyw, Graf_macierz*wzor)
{
	
	//int j=0;
	int ktora;
	for(int i=0;i<this->wiersze;i++)
	{
		for(int j=0;j<this->ile_krawedzi;j++)
		{
			//sprawdzenie gdzie zaczyna (zmienna i) i ko�czy si� (zmienna k) kraw�d�
			if(this->macierz[i][j].wartosc==1)
			{
				for(int k=0;k<this->wiersze;k++)
				{
					if(macierz[k][j].wartosc==-1)
					{
						ktora=k;
						break;
					}

				}
				this->macierz[i][j].waga=wzor->macierz[i][j].waga-przeplyw[i][ktora];
				//this->macierz[ktora][j].waga=wzor->macierz[ktora][j].waga-przeplyw[ktora][i];
			}
		}
	}
}