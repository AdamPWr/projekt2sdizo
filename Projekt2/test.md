# Projekt SDiZO 2

## Badanie struktur grafowych.

### Adam Cierniak, 241310

<br>

## Wstęp

### Cel projektu


Zbadanie efektywności algorytmów grafowych: wyznaczanie minimalnego drzewa rozpinającego oraz najkrótszych ścieżek w zależności od
rozmiaru instancji oraz sposobu reprezentacji w pamięci komputera: macierz sąsiedztwa, lista sąsiedztwa.
Program stworzony do badań został napisany w języku C++. Czas mierzony był za biblioteki chrono.
Liczby wierzchołków użyte do testowania to 10, 20, 30, 40, 50, natomiast gęstości grafów to 25%, 50%, 75%, 99%.
Do procesu generacji użyte zostały funkcje z biblioteki random, dzięki czemu liczy losowane były z rozkładem jednostajnym.
Wagi krawędzi losowane były z przedziału w przypadku wag nieujemnych, natomiast w przypadku wag ujemnych z przedziału <0, 100>.
Generacja grafu polegała na losowaniu wierzchołka początkowego, a następnie wierzchołka końcowego dopóki wylosuje się wartość wierzchołka niepołączonego już z wierzchołkiem początkowym.


### Zaimplementowane algorytmy


* Prima -  algorytm zachłanny wyznaczający tzw. minimalne drzewo rozpinające (MDR). Mając do dyspozycji graf nieskierowany i spójny, tzn. taki w którym krawędzie grafu nie mają ustalonego kierunku oraz
 dla każdych dwóch wierzchołków grafu istnieje droga pomiędzy nimi, algorytm oblicza podzbiór E′ zbioru krawędzi E, dla którego graf nadal pozostaje spójny, ale suma kosztów wszystkich krawędzi zbioru E′
jest najmniejsza możliwa

* Kruskala - algorytm grafowy wyznaczający minimalne drzewo rozpinające dla grafu nieskierowanego ważonego, o ile jest on spójny. Innymi słowy, znajduje drzewo zawierające wszystkie wierzchołki grafu, którego waga jest najmniejsza możliwa.

* Dikstry - mając dany graf z wyróżnionym wierzchołkiem (źródłem) algorytm znajduje odległości od źródła do wszystkich pozostałych wierzchołków.
Łatwo zmodyfikować go tak, aby szukał wyłącznie najkrótszej ścieżki do jednego ustalonego wierzchołka, po prostu przerywając działanie w momencie dojścia do wierzchołka docelowego.

* Belmana Forda - algorytm służący do wyszukiwania najkrótszych ścieżek w grafie ważonym z wierzchołka źródłowego do wszystkich pozostałych wierzchołków




## Sposób testowania


Każdy algorytm został przetestowany dla 10, 20, 30, 40 i 50 wierzchołków, z zapełnieniem 25, 50, 75 i 99-procentowym.

Wygenerowane zostało po 100 instancji każdego z probemów, a średnie poszczególnych czasów zostały naniesione na wykresy.


Program wyświetla każdy wygenerowany graf i rozwiązanie każdego algorytmu, można więc sprawdzić poprawność działania chodźby za pomocą ołówka i kartki papieru.




## Wykresy - typ 1



Osobne dla każdej reprezentacji grafu (macierz, lista).
8 linii, po jednej dla każdej pary (Typ algorytmu, gęstośc).



![](plots/type_1_dijkstra_belman_list.png)

![](plots/type_1_dijkstra_belman_matrix.png)

![](plots/type_1_prim_kruskal_list.png)

![](plots/type_1_prim_kruskal_matrix.png)

<br><br><br><br><br><br><br><br><br><br><br><br>

## Wykresy - typ 2


Osobne dla każdej gęstości grafu - porównują wydajność macierzowej i listowej implementacji grafu.


### Wyszukiwanie ścieżki - Dijkstra/Belman-Ford

![](plots/type_2_dijkstra_belman_25.png)

![](plots/type_2_dijkstra_belman_50.png)

![](plots/type_2_dijkstra_belman_75.png)

![](plots/type_2_dijkstra_belman_99.png)

<br><br><br><br><br><br><br><br><br><br><br><br>

### MST - Prim/Kruskal

![](plots/type_2_prim_kruskal_25.png)

![](plots/type_2_prim_kruskal_50.png)

![](plots/type_2_prim_kruskal_75.png)

![](plots/type_2_prim_kruskal_99.png)

## Wnioski


Testy algorytmów dały spodziewane wyniki, to znaczy takie jakie wynikały z ich złożoności obliczeniowej.
Podczas testowania czasu wykonania  algorytmu Dijkstry i Belmana-Forda, dla gęstości wypełnienia 99%, linie na wykresie 
nałożyły się na siebie. Próbowałem poprawić to poprzez zmianę rozdzielczości osi Y jednak wykres stawał się wtedy mało czytelny
dlatego zostawiłem to w stanie takim jak wyżej.


