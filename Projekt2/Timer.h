# pragma once
#include<chrono>

using timePoint = std::chrono::steady_clock::time_point;

class Timer
{
    private:

    timePoint begin;
    timePoint end;

    public:
    Timer() = default;


    void stop();
    void start();
    double getTimeInMicroSec();
};